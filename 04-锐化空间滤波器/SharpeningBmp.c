#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
/*
位图头结构
*/
#pragma pack(1)
typedef struct tagBITMAPFILEHEADER
{
	unsigned char bfType[2];//文件格式
	unsigned long bfSize;//文件大小
	unsigned short bfReserved1;//保留
	unsigned short bfReserved2;
	unsigned long bfOffBits; //DIB数据在文件中的偏移量
}fileHeader;
#pragma pack()
/*
位图数据信息结构
*/
#pragma pack(1)
typedef struct tagBITMAPINFOHEADER
{
	unsigned long biSize;//该结构的大小
	long biWidth;//文件宽度
	long biHeight;//文件高度
	unsigned short biPlanes;//平面数
	unsigned short biBitCount;//颜色位数
	unsigned long biCompression;//压缩类型
	unsigned long biSizeImage;//DIB数据区大小
	long biXPixPerMeter;
	long biYPixPerMeter;
	unsigned long biClrUsed;//多少颜色索引表
	unsigned long biClrImporant;//多少重要颜色
}fileInfo;
#pragma pack()
/*
调色板结构
*/
#pragma pack(1)
typedef struct tagRGBQUAD
{
	unsigned char rgbBlue; //蓝色分量亮度
	unsigned char rgbGreen;//绿色分量亮度
	unsigned char rgbRed;//红色分量亮度
	unsigned char rgbReserved;
}rgbq;
#pragma pack()

int main()
{
	int i, j, k;
	FILE * fpBMP, *fpSharpen;
	fileHeader * fh;
	fileInfo * fi;
	rgbq * fq;

	if ((fpBMP = fopen("D:/bmpTest/33.bmp", "rb")) == NULL)
	{
		printf("打开文件失败");
		exit(0);
	}

	if ((fpSharpen = fopen("D:/bmpTest/35.bmp", "wb")) == NULL)
	{
		printf("创建文件失败");
		exit(0);
	}

	fh = (fileHeader *)malloc(sizeof(fileHeader));
	fi = (fileInfo *)malloc(sizeof(fileInfo));
	//读取位图头结构和信息头
	fread(fh, sizeof(fileHeader), 1, fpBMP);
	fread(fi, sizeof(fileInfo), 1, fpBMP);
	//修改头信息
	fi->biBitCount = 8;
	fi->biSizeImage = ((fi->biWidth * 3 + 3) / 4) * 4 * fi->biHeight;

	fh->bfOffBits = sizeof(fileHeader)+sizeof(fileInfo)+256 * sizeof(rgbq);
	fh->bfSize = fh->bfOffBits + fi->biSizeImage;

	//创建调色版
	fq = (rgbq *)malloc(256 * sizeof(rgbq));
	for (i = 0; i<256; i++)
	{
		fq[i].rgbBlue = fq[i].rgbGreen = fq[i].rgbRed = i;
	}
	//将头信息写入
	fwrite(fh, sizeof(fileHeader), 1, fpSharpen);
	fwrite(fi, sizeof(fileInfo), 1, fpSharpen);
	fwrite(fq, sizeof(rgbq), 256, fpSharpen);

	//8位图像不读取颜色查找表.
	fseek(fpBMP, sizeof(rgbq)* 256, SEEK_CUR);

	long ImgWidth = fi->biWidth;
	long ImgHeight = fi->biHeight;

	unsigned char ** ImgData;//存储原图像的每个点的灰度值.
	unsigned char ** ImgData2;//滤波后每个像素点的灰度值.
	ImgData = (unsigned char **)malloc(sizeof(unsigned char*)*ImgHeight);
	ImgData2 = (unsigned char **)malloc(sizeof(unsigned char*)*ImgHeight);
	for (int i = 0; i < fi->biHeight; i++)
	{
		ImgData[i] = (unsigned char*)malloc(sizeof(unsigned char*)*ImgWidth);
		ImgData2[i] = (unsigned char*)malloc(sizeof(unsigned char*)*ImgWidth);
	}

	for (i = 0; i < ImgHeight; i++)
	{
		for (j = 0; j < (ImgWidth + 3) / 4 * 4; j++)
		{
			//读取灰度值.
			fread(&ImgData[i][j], 1, 1, fpBMP);
		}
	}

	//图像边界直接写入.
	for (int i = 0; i < ImgWidth; i++)
	{
		ImgData2[0][i] = ImgData[0][i];
		ImgData2[ImgHeight - 1][i] = ImgData[ImgHeight - 1][i];
	}
	for (int i = 0; i < ImgHeight; i++)
	{
		ImgData2[i][0] = ImgData[i][0];
		ImgData2[i][ImgWidth - 1] = ImgData[i][ImgWidth - 1];
	}

	//模板1
	int sharpenModel1[9] = {
		0, 1, 0,
		1, -4, 1,
		0, 1, 0
	};

	//模板2
	int sharpenModel2[9] = {
		1, 1, 1,
		1, -8, 1,
		1, 1, 1
	};

	int* model = sharpenModel2;
	for (int i = 1; i < ImgHeight - 1; i++)
	{
		for (int j = 1; j < ImgWidth - 1; j++)
		{
			
			int sharpening = (model[0] * ImgData[i - 1][j - 1] + model[1] * ImgData[i - 1][j] + model[2] * ImgData[i - 1][j + 1]
				+ model[3] * ImgData[i][j - 1] + model[4] * ImgData[i][j] + model[5] * ImgData[i][j + 1]
				+ model[6] * ImgData[i + 1][j - 1] + model[7] * ImgData[i + 1][j] + model[8] * ImgData[i + 1][j + 1]);
			sharpening += ImgData[i][j];
			if (sharpening < 0)
			{
				sharpening = 0;
			}
			else if (sharpening > 255)
			{
				sharpening = 255;
			}
			ImgData2[i][j] = sharpening;
		}
	}

	//将灰度图信息写入
	for (int i = 0; i < ImgHeight; i++)
	{
		fwrite(ImgData2[i], ImgWidth, 1, fpSharpen);
	}

	free(fh);
	free(fi);
	free(fq);
	fclose(fpBMP);
	fclose(fpSharpen);
	printf("success\n");
	return 0;
}
