#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
/*
位图头结构
*/
#pragma pack(1)
typedef struct tagBITMAPFILEHEADER
{
	unsigned char bfType[2];//文件格式
	unsigned long bfSize;//文件大小
	unsigned short bfReserved1;//保留
	unsigned short bfReserved2;
	unsigned long bfOffBits; //DIB数据在文件中的偏移量
}fileHeader;
#pragma pack()
/*
位图数据信息结构
*/
#pragma pack(1)
typedef struct tagBITMAPINFOHEADER
{
	unsigned long biSize;//该结构的大小
	long biWidth;//文件宽度
	long biHeight;//文件高度
	unsigned short biPlanes;//平面数
	unsigned short biBitCount;//颜色位数
	unsigned long biCompression;//压缩类型
	unsigned long biSizeImage;//DIB数据区大小
	long biXPixPerMeter;
	long biYPixPerMeter;
	unsigned long biClrUsed;//多少颜色索引表
	unsigned long biClrImporant;//多少重要颜色
}fileInfo;
#pragma pack()
/*
调色板结构
*/
#pragma pack(1)
typedef struct tagRGBQUAD
{
	unsigned char rgbBlue; //蓝色分量亮度
	unsigned char rgbGreen;//绿色分量亮度
	unsigned char rgbRed;//红色分量亮度
	unsigned char rgbReserved;
}rgbq;
#pragma pack()

void avgSmoothing(unsigned char ** ImgData, unsigned char ** ImgData2, int ImgWidth, int ImgHeight);
void midSmoothing(unsigned char ** ImgData, unsigned char ** ImgData2, int ImgWidth, int ImgHeight);

int main()
{
	int i, j, k;
	FILE * fpBMP, *fpSmooth;
	fileHeader * fh;
	fileInfo * fi;
	rgbq * fq;

	if ((fpBMP = fopen("D:/bmpTest/32.bmp", "rb")) == NULL)
	{
		printf("打开文件失败");
		exit(0);
	}

	if ((fpSmooth = fopen("D:/bmpTest/34.bmp", "wb")) == NULL)
	{
		printf("创建文件失败");
		exit(0);
	}

	fh = (fileHeader *)malloc(sizeof(fileHeader));
	fi = (fileInfo *)malloc(sizeof(fileInfo));
	//读取位图头结构和信息头
	fread(fh, sizeof(fileHeader), 1, fpBMP);
	fread(fi, sizeof(fileInfo), 1, fpBMP);
	//修改头信息
	fi->biBitCount = 8;
	fi->biSizeImage = ((fi->biWidth * 3 + 3) / 4) * 4 * fi->biHeight;

	fh->bfOffBits = sizeof(fileHeader)+sizeof(fileInfo)+256 * sizeof(rgbq);
	fh->bfSize = fh->bfOffBits + fi->biSizeImage;

	//创建调色版
	fq = (rgbq *)malloc(256 * sizeof(rgbq));
	for (i = 0; i<256; i++)
	{
		fq[i].rgbBlue = fq[i].rgbGreen = fq[i].rgbRed = i;
	}
	//将头信息写入
	fwrite(fh, sizeof(fileHeader), 1, fpSmooth);
	fwrite(fi, sizeof(fileInfo), 1, fpSmooth);
	fwrite(fq, sizeof(rgbq), 256, fpSmooth);

	//8位图像不读取颜色查找表.
	fseek(fpBMP, sizeof(rgbq)* 256, SEEK_CUR);

	long ImgWidth = fi->biWidth;
	long ImgHeight = fi->biHeight;

	unsigned char ** ImgData;//存储原图像的每个点的灰度值.
	unsigned char ** ImgData2;//滤波后每个像素点的灰度值.
	ImgData = (unsigned char **)malloc(sizeof(unsigned char*)*ImgHeight);
	ImgData2 = (unsigned char **)malloc(sizeof(unsigned char*)*ImgHeight);
	for (int i = 0; i < fi->biHeight; i++)
	{
		ImgData[i] = (unsigned char*)malloc(sizeof(unsigned char*)*ImgWidth);
		ImgData2[i] = (unsigned char*)malloc(sizeof(unsigned char*)*ImgWidth);
	}

	for (i = 0; i < ImgHeight; i++)
	{
		for (j = 0; j < (ImgWidth + 3) / 4 * 4; j++)
		{
			//读取灰度值.
			fread(&ImgData[i][j], 1, 1, fpBMP);
		}
	}

	//avgSmoothing(ImgData, ImgData2, ImgWidth, ImgHeight);
	midSmoothing(ImgData, ImgData2, ImgWidth, ImgHeight);

	//将灰度图信息写入
	for (int i = 0; i < ImgHeight; i++)
	{
		fwrite(ImgData2[i], ImgWidth, 1, fpSmooth);
	}

	free(fh);
	free(fi);
	free(fq);
	fclose(fpBMP);
	fclose(fpSmooth);
	printf("success\n");
	return 0;
}

//均值滤波器.
//ImgData:原图像数据 ImgData2:滤波后的图像数据 ImgWidth:图像宽度 ImgHeight:图像高度
void avgSmoothing(unsigned char ** ImgData, unsigned char ** ImgData2, int ImgWidth, int ImgHeight)
{
	//对图像的四个角点操作.
	ImgData2[0][0] = (unsigned char)((float)(ImgData[0][0] + ImgData[0][1] + ImgData[1][0] + ImgData[1][1]) / 4 + 0.5);
	ImgData2[0][ImgWidth - 1] = (unsigned char)((float)(ImgData[0][ImgWidth - 1] + ImgData[ImgWidth - 2][ImgWidth - 1] + ImgData[ImgWidth - 2][ImgWidth - 2] + ImgData[1][ImgWidth - 1]) / 4 + 0.5);
	ImgData2[ImgHeight - 1][0] = (unsigned char)((float)(ImgData[ImgHeight - 1][0] + ImgData[ImgHeight - 1][1] + ImgData[ImgHeight - 2][0] + ImgData[ImgHeight - 2][1]) / 4 + 0.5);
	ImgData2[ImgHeight - 1][ImgWidth - 1] = (unsigned char)((float)(ImgData[ImgHeight - 1][ImgWidth - 1] + ImgData[ImgHeight - 2][ImgWidth - 1] + ImgData[ImgHeight - 2][ImgWidth - 2] + ImgData[ImgHeight - 1][ImgWidth - 2]) / 4 + 0.5);

	//图像边界的操作.
	for (int i = 1; i < ImgWidth - 1; i++)
	{
		//第一行.
		ImgData2[0][i] = (unsigned char)((float)(ImgData[0][i - 1] + ImgData[0][i] + ImgData[0][i + 1]
			+ ImgData[1][i - 1] + ImgData[1][i] + ImgData[1][i + 1]) / 6+0.5);
		//最后一行.
		ImgData2[ImgHeight - 1][i] = (unsigned char)((float)(ImgData[ImgHeight - 2][i - 1] + ImgData[ImgHeight - 2][i] + ImgData[ImgHeight - 2][i + 1]
			+ ImgData[ImgHeight - 1][i - 1] + ImgData[ImgHeight - 1][i] + ImgData[ImgHeight - 1][i + 1]) / 6+0.5);
	}
	for (int i = 1; i < ImgHeight - 1; i++)
	{
		//第一列.
		ImgData2[i][0] = (unsigned char)((float)(ImgData[i - 1][0] + ImgData[i - 1][1]
			+ ImgData[i][0] + ImgData[i][1]
			+ ImgData[i + 1][0] + ImgData[i + 1][1]) / 6+0.5);
		//最后一列.
		ImgData2[i][ImgWidth - 1] = (unsigned char)((float)(ImgData[i - 1][ImgWidth - 2] + ImgData[i - 1][ImgWidth - 1]
			+ ImgData[i][ImgWidth - 2] + ImgData[i][ImgWidth - 1]
			+ ImgData[i + 1][ImgWidth - 2] + ImgData[i + 1][ImgWidth - 1]) / 6+0.5);
	}

	//对中间的像素点进行附近9格均值滤波处理.
	for (int i = 1; i < ImgHeight - 1; i++)
	{
		for (int j = 1; j < ImgWidth - 1; j++)
		{
			//均值滤波.
			ImgData2[i][j] = (unsigned char)((float)(ImgData[i - 1][j + 1] + ImgData[i][j + 1] + ImgData[i + 1][j + 1]
				+ ImgData[i - 1][j] + ImgData[i][j] + ImgData[i + 1][j]
				+ ImgData[i - 1][j - 1] + ImgData[i][j - 1] + ImgData[i + 1][j - 1]) / 9+0.5);
		}
	}
}

//取数组中的中值.
unsigned char getMidNum(unsigned char* allNum, int length)
{
	int midIndex = length / 2 +1;//这里下标+1后效果更好.
	for (int i = 0; i < midIndex - 1; i++)
	{
		for (int j = 0; j < length - 1 - i; j++)// j开始等于0
		{
			if (allNum[j] < allNum[j + 1])
			{
				int temp = allNum[j];
				allNum[j] = allNum[j + 1];
				allNum[j + 1] = temp;
			}
		}
	}
	return allNum[midIndex];
}

//中值滤波器.
void midSmoothing(unsigned char ** ImgData, unsigned char ** ImgData2, int ImgWidth, int ImgHeight)
{
	//这里对数组赋值有没有有效一点的方法啊
	unsigned char tempData[9] = { ImgData[0][0], ImgData[0][1], ImgData[1][0], ImgData[1][1] };
	//对图像的四个角点操作.
	ImgData2[0][0] = getMidNum(tempData, 4);

	tempData[0] = ImgData[0][ImgWidth - 1];
	tempData[1] = ImgData[ImgWidth - 2][ImgWidth - 1];
	tempData[2] = ImgData[ImgWidth - 2][ImgWidth - 2];
	tempData[3] = ImgData[1][ImgWidth - 1];
	ImgData2[0][ImgWidth - 1] = getMidNum(tempData, 4);

	tempData[0] = ImgData[ImgHeight - 1][0];
	tempData[1] = ImgData[ImgHeight - 1][1];
	tempData[2] = ImgData[ImgHeight - 2][0];
	tempData[3] = ImgData[ImgHeight - 2][1];
	ImgData2[ImgHeight - 1][0] = getMidNum(tempData, 4);
	
	tempData[0] = ImgData[ImgHeight - 1][ImgWidth - 1];
	tempData[1] = ImgData[ImgHeight - 2][ImgWidth - 1];
	tempData[2] = ImgData[ImgHeight - 2][ImgWidth - 2];
	tempData[3] = ImgData[ImgHeight - 1][ImgWidth - 2];
	ImgData2[ImgHeight - 1][ImgWidth - 1] = getMidNum(tempData, 4);
	
	//图像边界的操作.
	for (int i = 1; i < ImgWidth - 1; i++)
	{
		//第一行.
		tempData[0] = ImgData[0][i - 1];
		tempData[1] = ImgData[0][i];
		tempData[2] = ImgData[0][i + 1];
		tempData[3] = ImgData[1][i - 1];
		tempData[4] = ImgData[1][i];
		tempData[5] = ImgData[1][i + 1];
		ImgData2[0][i] = getMidNum(tempData, 6);
		//最后一行.
		tempData[0] = ImgData[ImgHeight - 2][i - 1];
		tempData[1] = ImgData[ImgHeight - 2][i];
		tempData[2] = ImgData[ImgHeight - 2][i + 1];
		tempData[3] = ImgData[ImgHeight - 1][i - 1];
		tempData[4] = ImgData[ImgHeight - 1][i];
		tempData[5] = ImgData[ImgHeight - 1][i + 1];
		ImgData2[ImgHeight - 1][i] = getMidNum(tempData, 6);
	}

	for (int i = 1; i < ImgHeight - 1; i++)
	{
		//第一列.
		tempData[0] = ImgData[i - 1][0];
		tempData[1] = ImgData[i - 1][1];
		tempData[2] = ImgData[i][0];
		tempData[3] = ImgData[i][1];
		tempData[4] = ImgData[i + 1][0];
		tempData[5] = ImgData[i + 1][1];
		ImgData2[i][0] = getMidNum(tempData, 6);
		//最后一列.
		tempData[0] = ImgData[i - 1][ImgWidth - 2];
		tempData[1] = ImgData[i - 1][ImgWidth - 1];
		tempData[2] = ImgData[i][ImgWidth - 2];
		tempData[3] = ImgData[i][ImgWidth - 1];
		tempData[4] = ImgData[i + 1][ImgWidth - 2];
		tempData[5] = ImgData[i + 1][ImgWidth - 1];
		ImgData2[i][ImgWidth - 1] = getMidNum(tempData, 6);
	}

	//对中间的像素点进行附近9格均值滤波处理.
	for (int i = 1; i < ImgHeight - 1; i++)
	{
		for (int j = 1; j < ImgWidth - 1; j++)
		{
			//均值滤波.
			tempData[0] = ImgData[i - 1][j + 1];
			tempData[1] = ImgData[i][j + 1];
			tempData[2] = ImgData[i + 1][j + 1];
			tempData[3] = ImgData[i - 1][j];
			tempData[4] = ImgData[i][j];
			tempData[5] = ImgData[i + 1][j];
			tempData[6] = ImgData[i - 1][j - 1];
			tempData[7] = ImgData[i][j - 1];
			tempData[8] = ImgData[i + 1][j - 1];
			ImgData2[i][j] = getMidNum(tempData, 9);
		}
	}
}
