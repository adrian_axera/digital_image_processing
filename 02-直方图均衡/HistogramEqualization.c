#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
/*
位图头结构
*/
#pragma pack(1)
typedef struct tagBITMAPFILEHEADER
{
	unsigned char bfType[2];//文件格式
	unsigned long bfSize;//文件大小
	unsigned short bfReserved1;//保留
	unsigned short bfReserved2;
	unsigned long bfOffBits; //DIB数据在文件中的偏移量
}fileHeader;
#pragma pack()
/*
位图数据信息结构
*/
#pragma pack(1)
typedef struct tagBITMAPINFOHEADER
{
	unsigned long biSize;//该结构的大小
	long biWidth;//文件宽度
	long biHeight;//文件高度
	unsigned short biPlanes;//平面数
	unsigned short biBitCount;//颜色位数
	unsigned long biCompression;//压缩类型
	unsigned long biSizeImage;//DIB数据区大小
	long biXPixPerMeter;
	long biYPixPerMeter;
	unsigned long biClrUsed;//多少颜色索引表
	unsigned long biClrImporant;//多少重要颜色
}fileInfo;
#pragma pack()
/*
调色板结构
*/
#pragma pack(1)
typedef struct tagRGBQUAD
{
	unsigned char rgbBlue; //蓝色分量亮度
	unsigned char rgbGreen;//绿色分量亮度
	unsigned char rgbRed;//红色分量亮度
	unsigned char rgbReserved;
}rgbq;
#pragma pack()

int main()
{
	/*存储RGB图像的一行像素点*/
	unsigned char ImgData[3000][3];
	/*将灰度图的像素存到一个一维数组中*/
	unsigned char ImgData2[3000];
	int i, j, k;
	FILE * fpBMP, *fpHis;
	fileHeader * fh;
	fileInfo * fi;
	rgbq * fq;
	int imgBit = 8;//原始图像位数.

	if ((fpBMP = fopen("D:/bmpTest/52.bmp", "rb")) == NULL)
	{
		printf("打开文件失败");
		exit(0);
	}

	if ((fpHis = fopen("D:/bmpTest/53.bmp", "wb")) == NULL)
	{
		printf("创建文件失败");
		exit(0);
	}

	fh = (fileHeader *)malloc(sizeof(fileHeader));
	fi = (fileInfo *)malloc(sizeof(fileInfo));
	//读取位图头结构和信息头
	fread(fh, sizeof(fileHeader), 1, fpBMP);
	fread(fi, sizeof(fileInfo), 1, fpBMP);
	imgBit = fi->biBitCount;
	//修改头信息
	fi->biBitCount = 8;
	fi->biSizeImage = ((fi->biWidth * 3 + 3) / 4) * 4 * fi->biHeight;
	//fi->biClrUsed=256;

	fh->bfOffBits = sizeof(fileHeader)+sizeof(fileInfo)+256 * sizeof(rgbq);
	fh->bfSize = fh->bfOffBits + fi->biSizeImage;

	//创建调色版
	fq = (rgbq *)malloc(256 * sizeof(rgbq));
	for (i = 0; i<256; i++)
	{
		fq[i].rgbBlue = fq[i].rgbGreen = fq[i].rgbRed = i;
		//fq[i].rgbReserved=0;
	}
	//将头信息写入
	fwrite(fh, sizeof(fileHeader), 1, fpHis);
	fwrite(fi, sizeof(fileInfo), 1, fpHis);
	fwrite(fq, sizeof(rgbq), 256, fpHis);
	if (imgBit == 8)
	{
		fseek(fpBMP, sizeof(rgbq)* 256, SEEK_CUR);
	}

	int totalPix = fi->biHeight * fi->biWidth;
	printf("totalPix:%d\n", totalPix);

	unsigned char* picGray;
	picGray = (unsigned char *)malloc(totalPix * sizeof(unsigned char));
	int picIndex = 0;
	long int originGray[256];
	for (int i = 0; i < 256; i++)
	{
		originGray[i] = 0;
	}
	for (int i = 0; i < fi->biHeight; i++)
	{
		for (j = 0; j<(fi->biWidth + 3) / 4 * 4; j++)
		{
			if (imgBit == 8)
			{
				fread(&ImgData[j], 1, 1, fpBMP);
			}
			else if (imgBit == 24)
			{
				for (k = 0; k<3; k++)
					fread(&ImgData[j][k], 1, 1, fpBMP);
			}
		}
		for (j = 0; j<(fi->biWidth + 3) / 4 * 4; j++)
		{
			//把灰度值加入数组.
			originGray[(int)((ImgData[j][0] + ImgData[j][1] + ImgData[j][2]) / 3)] += 1;
			picGray[picIndex] = (int)((ImgData[j][0] + ImgData[j][1] + ImgData[j][2]) / 3);
			picIndex++;
		}
	}
	printf("picIndex:%d\n", picIndex);
	float originGrayP[256];//原始图像灰度值概率.
	for (int i = 0; i < 256; i++)
	{
		originGrayP[i] = (float)originGray[i] / totalPix;
		printf("%d ", originGray[i]);
	}
	float graySum = 0;
	for (int i = 0; i < 256; i++)
	{
		graySum += originGrayP[i];
	}
	printf("%f", graySum);//0.05
	unsigned char histogram[256];//原来的灰度值对应的直方图均衡后的灰度值.
	for (int i = 0; i < 256; i++)
	{
		float sum = 0.0;
		for (int j = 0; j <= i; j++)
		{
			sum += originGrayP[j];
		}
		histogram[i] = (int)(255 * sum + 0.5);
	}
	for (int i = 0; i < 256; i++)
	{
		printf("%d均衡后的灰度值:%d\n", i, histogram[i]);
	}
	picIndex = 0;
	for (i = 0; i<fi->biHeight; i++)
	{

		for (j = 0; j<(fi->biWidth + 3) / 4 * 4; j++)
		{
			ImgData2[j] = histogram[picGray[picIndex]];
			picIndex++;
		}
		//将直方图均衡灰度图信息写入
		fwrite(ImgData2, j, 1, fpHis);
	}

	free(fh);
	free(fi);
	free(fq);
	fclose(fpBMP);
	fclose(fpHis);
	printf("success\n");
	return 0;
}
